import { IServerConfiguration } from '../../../../modules/interfaces';
import * as path from 'path';

const config: IServerConfiguration = {
    env: 'preproduction',
    serverId: 'server.api',
    port: 9000,
    host: 'http://localhost:9000',
    mongodb:'mongodb://127.0.0.1:27017/server-api-preprod'
};

export default config;
