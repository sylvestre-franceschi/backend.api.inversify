import { IServerConfiguration } from '../../../../modules/interfaces';
import * as path from 'path';

const config: IServerConfiguration = {
	env: 'development',
	serverId: 'server.api',
	port: 9337,
	host: 'http://localhost:9337',
	mongodb:'mongodb://127.0.0.1:27017/server-api-dev'
};

export default config;
