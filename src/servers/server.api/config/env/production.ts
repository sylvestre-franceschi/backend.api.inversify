import { IServerConfiguration } from '../../../../modules/interfaces';
import * as path from 'path';

const config: IServerConfiguration = {
    env: 'production',
    serverId: 'server.api',
    port: 8000,
    host: 'http://localhost:8000',
    mongodb:'mongodb://127.0.0.1:27017/server-api'
};

export default config;
