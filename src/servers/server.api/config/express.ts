import { Application } from 'express';

import container from './di';
import * as bodyParser from 'body-parser';
import config from './env';
import { useServiceClient } from '../../../modules/clients/index';
const cors = require('cors');


export function configureExpress(app: Application) {

    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));

    // disable 'X-Powered-By' header in response
    app.disable('x-powered-by');

    // enable CORS - Cross Origin Resource Sharing
    app.use(cors());

    useServiceClient(app, container);
}