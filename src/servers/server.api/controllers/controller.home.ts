import { httpGet, httpPost, controller } from 'inversify-express-utils';
import { injectable, inject } from 'inversify';
import { authorize, authorizeAdmin } from '../../../modules/common';

import { Request, Response, NextFunction } from 'express';

@controller('/')
@injectable()
export class ControllerHome {
    constructor(
    ) {
    }

    @httpGet('/')
    public get(req: Request, res: Response, next: NextFunction): any {
        return `server.api server is running!`;
    }
}
