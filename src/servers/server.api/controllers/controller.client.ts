import { httpGet, httpPost, httpPut, httpDelete, controller } from 'inversify-express-utils';

import { injectable, inject } from 'inversify';

import { Request, Response, NextFunction } from 'express';
import { validateBody, validateQuery, authorize, authorizeAdmin, validate } from '../../../modules/common';
import {
    ServiceClient,
} from '../../../modules/clients';
import {
    FindClientIdRequest,
    RemoveClientIdRequest,
    CreateClientRequest,
    EditClientRequest,
} from '../../../modules/clients/models';

import { TYPES } from '../../../modules/common/';

@injectable()
@controller('/client'/* , authorize() */)
export class ControllerClient {
    constructor(
        @inject(TYPES.ServiceClient) private serviceClient: ServiceClient
        ) { }


    @httpPost('/find/id', validateBody(FindClientIdRequest))
    public async getClient(request: Request) {
        const resp = await this.serviceClient.find(request.body);
        if (!resp)
            return 400;
        return resp;
    }

    @httpPost('/find/filter')
    public async getFilter(request: Request) {
        const resp = await this.serviceClient.getFilter(request.body);
        if (!resp)
            return 400;
        return resp;
    }


    @httpPost('/', validateBody(CreateClientRequest))
    public async create(request: Request) {
        const resp = await this.serviceClient.create(request.body);
        if (!resp)
            return 400;
        return resp;
    }

    @httpPut('/', validateBody(EditClientRequest))
    public async update(request: Request) {
        const resp = await this.serviceClient.update(request.body);
        if (!resp)
            return 400;
        return resp;
    }

    @httpDelete('/', validateBody(RemoveClientIdRequest))
    public async remove(request: Request) {
        const resp = await this.serviceClient.remove(request.body);
        if (!resp)
            return 400;
        return resp;
    }

}

