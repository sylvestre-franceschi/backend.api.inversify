import { Db, ObjectID } from 'mongodb';
import { injectable } from 'inversify';
import { MongoDBConnection } from '../mongo.db.connection';
import * as mongo from 'mongodb';

@injectable()
export class MongoDBClient {
  public db: Db;
  private pageSize: number;

  constructor(mongodb) {
    MongoDBConnection.getConnection(mongodb, (connection) => {
      this.db = connection;
      this.pageSize = 100;
    });
  }

  public find(collection: string, filter: Object, result: (error, data) => void): void {
    this.db.collection(collection).find(filter).toArray((error, find) => {
      return result(error, find);
    });
  }

  public findPage(collection: string, filter: Object, result: (error, data) => void): void {
    this.db.collection(collection).find(filter).limit(this.pageSize).toArray((error, find) => {
      return result(error, find);
    });
  }

  public findOneById(collection: string, objectId: string, result: (error, data) => void): void {
    this.db.collection(collection).find({ _id: new ObjectID(objectId) }).limit(1).toArray((error, find) => {
      return result(error, find[0]);
    });
  }

  public findOneByUid(collection: string, uid: string, result: (error, data) => void): void {
    this.db.collection(collection).find({ uid: uid }).limit(1).toArray((error, find) => {
      return result(error, find[0]);
    });
  }

  public findOneByUuid(collection: string, uuid: string, result: (error, data) => void): void {
    this.db.collection(collection).find({ uuid: uuid }).limit(1).toArray((error, find) => {
      return result(error, find[0]);
    });
  }

  public findOneByFilter(collection: string, filter: object, result: (error, data) => void): void {
    this.db.collection(collection).find(filter).limit(1).toArray((error, find) => {
      return result(error, find[0]);
    });
  }

  public insert(collection: string, model: Object, result: (error, data) => void): void {
    this.db.collection(collection).insertOne(model, (error, insert) => {
      return result(error, insert.ops[0]);
    });
  }

  public update(collection: string, objectId: string, model: Object, result: (error, data) => void): void {
    this.db.collection(collection).updateOne({ _id: new ObjectID(objectId) }, { $set: model }, (error, update) => {
      return result(error, model);
    });
  }

  public updateByUid(collection: string, uid: string, model: Object, result: (error, data) => void): void {
    this.db.collection(collection).updateOne({ uid: uid }, { $set: model }, (error, update) => {
      return result(error, model);
    });
  }

  public updateByFilter(collection: string, filter: object, model: Object, result: (error, data) => void): void {
    this.db.collection(collection).updateOne(filter, { $set: model }, (error, update) => {
      return result(error, model);
    });
  }

  public remove(collection: string, objectId: string, result: (error, data) => void): void {
    this.db.collection(collection).deleteOne({ _id: new ObjectID(objectId) }, (error, remove) => {
      return result(error, remove);
    });
  }

  public removeByUid(collection: string, uid: string, result: (error, data) => void): void {
    this.db.collection(collection).deleteOne({ uid: uid }, (error, remove) => {
      return result(error, remove);
    });
  }

  public removeByFilter(collection: string, filter: object, result: (error, data) => void): void {
    this.db.collection(collection).deleteOne(filter, (error, remove) => {
      return result(error, remove);
    });
  }

  public getLastRecord(collection: string, result: (error, data) => void): void {
    this.db.collection(collection).find().sort({ _id: -1 }).limit(1).toArray((error, find) => {
      return result(error, find[0]);
    });
  }

  public getLastRecordByFilter(collection: string, filter: Object, result: (error, data) => void): void {
    this.db.collection(collection).find(filter).sort({ _id: -1 }).limit(1).toArray((error, find) => {
      return result(error, find[0]);
    });
  }


  public count(collection: string, filter: Object, result: (error, data) => void): void {
    this.db.collection(collection).count(filter, (error, find) => {
      return result(error, find);
    });
  }

}