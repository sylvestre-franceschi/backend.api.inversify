export const TYPES = {
    MongoDBClient: Symbol('MongoDBClient'),
    ServiceUser: Symbol('ServiceUser'),
    StoreUser: Symbol('StoreUser'),
    StoreClient: Symbol('StoreClient'),
    ServiceClient: Symbol('ServiceClient'),
};

export default TYPES;