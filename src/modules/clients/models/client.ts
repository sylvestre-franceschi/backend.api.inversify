import * as joi from 'joi';
import { Constraint } from '../../../modules/common';
import { ModelClient } from '../index';

export class FindClientIdRequest {
    @Constraint(joi.string().required())
    id: string;
}

export class RemoveClientIdRequest {
    @Constraint(joi.string().required())
    id: string;
}

export class CreateClientRequest {
    @Constraint(joi.string().required())
    name: string;
}

export class EditClientRequest {
    @Constraint(joi.string().required())
    id: string;
    @Constraint(joi.string().required())
    name: string;
}

export class ClientResponse {
    data: ModelClient | ModelClient[];
    error: string = "";
    code: number = 200;
}
