import { ObjectID } from 'mongodb';;
import { injectable, inject } from 'inversify';
import { ModelClient } from '../../models'
import { MongoDBClient, MongoDBConnection } from '../../../database';
import { TYPES } from '../../../common';

@injectable()
export class StoreClient {
    private collectionName: string;
    constructor(
        @inject(TYPES.MongoDBClient) private mongoClient: MongoDBClient,
    ) {
        this.collectionName = 'Client';
    }

    create(name: string): Promise<ModelClient> {
        return new Promise<ModelClient>(async (resolve) => {
            const newClient = new ModelClient();
            newClient.name = name;
            this.mongoClient.insert(this.collectionName, newClient, async (error, data: ModelClient) => {
                resolve(data);
            });
        });
    }

    get(id: string): Promise<ModelClient> {
        return new Promise<ModelClient>((resolve) => {
            this.mongoClient.findOneById(this.collectionName, id, (error, data: ModelClient) => {
                resolve(data);
            });
        });
    }

    edit(id: string, name: string): Promise<ModelClient> {
        return new Promise<ModelClient>(async (resolve) => {
            const editClient = await this.get(id);
            editClient.name = name;
            this.mongoClient.update(this.collectionName, id, editClient, async (error, data: ModelClient) => {
                resolve(data);
            });
        });
    }

    delete(id: string): Promise<boolean> {
        return new Promise<boolean>(async (resolve) => {
            this.mongoClient.remove(this.collectionName, id, async (error, data) => {
                if (data) {
                    resolve(true);
                }
                else
                    resolve(false);
            })
        });
    }

    getFilter(filter: object): Promise<ModelClient[]> {
        return new Promise<ModelClient[]>((resolve) => {
            this.mongoClient.find(this.collectionName, filter, (error, data: ModelClient[]) => {
                resolve(data);
            });
        });
    }

}