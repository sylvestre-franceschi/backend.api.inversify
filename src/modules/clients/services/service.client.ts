import { injectable, inject } from 'inversify';
import {
    ModelClient,
    CreateClientRequest,
    EditClientRequest,
    FindClientIdRequest,
    RemoveClientIdRequest,
    ClientResponse
} from '../models'

import { TYPES } from '../../../modules/common';
import {
    StoreClient
} from './stores';

@injectable()
export class ServiceClient {
    constructor(
        @inject(TYPES.StoreClient) private store: StoreClient
    ) {
    }


    async create(newClient: CreateClientRequest): Promise<ClientResponse> {
        const resp = new ClientResponse();
        const client = await this.store.create(newClient.name);
        if (client)
            resp.data = client;
        else {
            resp.data = null;
            resp.code = 100;
            resp.error = `Failed to create client ${newClient}`
        }
        return resp;
    }

    async find(findClient: FindClientIdRequest): Promise<ClientResponse> {
        const resp = new ClientResponse();
        const client = await this.store.get(findClient.id);
        if (client)
            resp.data = client;
        else {
            resp.data = null;
            resp.code = 101;
            resp.error = `Failed to find client with this id ${findClient.id}`
        }
        return resp;
    }

    async update(editClient: EditClientRequest): Promise<ClientResponse> {
        const resp = new ClientResponse();

        const client = await this.store.edit(editClient.id, editClient.name);
        if (client)
            resp.data = client;
        else {
            resp.data = null;
            resp.code = 101;
            resp.error = `Failed to update client with this id ${editClient.id}`
        }
        return resp;
    }

    async remove(removeClient: RemoveClientIdRequest): Promise<ClientResponse> {
        const resp = new ClientResponse();
        const client = await this.store.get(removeClient.id);
        if (!client) {
            resp.data = null;
            resp.code = 101;
            resp.error = `Failed to find client with this id ${removeClient.id}`;
            return resp;
        }
        else {
            const removed = await this.store.delete(removeClient.id)
            if (removed)
                resp.data = client;
            else {
                resp.data = null;
                resp.code = 101;
                resp.error = `Failed to remove client with this id ${removeClient.id}`
            }
            return resp;
        }

    }
    async getFilter(filter: object): Promise<ClientResponse> {
        const resp = new ClientResponse();
        const clients = await this.store.getFilter(filter);
        if (clients)
            resp.data = clients;
        else {
            resp.data = null;
            resp.code = 101;
            resp.error = `Failed to update client with this filter ${filter}`
        }
        return resp;
    }
}