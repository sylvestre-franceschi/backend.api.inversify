export * from './models';
export * from './services';

import { Application } from 'express';
import { Container } from 'inversify';
import {
    ServiceClient,
    StoreClient
} from './services';
import { TYPES } from '../../modules/common';

export function useServiceClient(app: Application, container: Container) {
    container.bind<StoreClient>(TYPES.StoreClient).to(StoreClient);
    container.bind<ServiceClient>(TYPES.ServiceClient).to(ServiceClient);
}
